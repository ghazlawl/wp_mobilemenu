# WP_MobileMenu

A jQuery plugin to create responsive navigation menus in WordPress using Treeview and the WordPress `wp_nav_menu()` function.

## Requirements

* [Treeview](http://bassistance.de/jquery-plugins/jquery-plugin-treeview/)
* [WordPress](http://wordpress.org/download/) (Version 3.5.0)
* [jQuery](http://jquery.com/download/)

## Demo

An interactive demo (should) be available [here](http://sandbox.endseven.net/wp/).

## How to Use

This plugin depends on the output of the WordPress `wp_nav_menu()` function (Admin -> Appearance -> Menus) to format the unordered lists. This plugin also depends on the Treeview jQuery plugin to expand and collapse the nested elements.

### jQuery

    $(document).ready(function()
    {

        $("#WP_MobileMenu").treeview(
        {
            animated: true,
            collapsed: true,
            persist: "location",
            unique: true
        });

        $("#WP_MobileMenu").mobilemenu(
        {
            wrapper: "WP_MobileMenuWrapper",
            collapsed: true,
            toggle_button: "WP_ToggleMobileMenu",
            close_button: "WP_CloseMobileMenu",
            before: ["", "", ""],
            after: ["&rsaquo;", "&rsaquo;", ""]
        });

    });

**Available Options:**

* wrapper - The id of the wrapper element,
* collapsed - Whether or not the menu is collapsed by default,
* toggle_button - The id of the toggle button (optional),
* close_button - The id of the close button (optional),
* before - An array of text to prepend to each level of links,
* after - An array of text to append to each level of links.

### PHP/HTML

The navigation menu is hidden by default using `display: none;` (in the CSS) and then made visible using `display: block;` (in the plugin). This stops the menu from being visible until `$(document).ready()` is called.

To toggle the visibility of the navigation menu using a toggle button, a toggle button must be created and assigned. The default id for the toggle button is `WP_ToggleMobileMenu`. If a toggle button isn't required, be sure to remove the `toggle_button: "WP_ToggleMobileMenu"` code from the plugin initialization block.

To close the navigation menu using the close button, a close button must be created and assigned. The default id for the close button is `WP_CloseMObileMenu`. If a close button isn't required, be sure to remove the `close_button: "WP_CloseMobileMenu"` code from the plugin initialization block as well as the code at the bottom of the menu.

A WordPress search form is included at the top of the navigation menu by default and is housed in a div called `WP_MobileMenuSearch`. The search form is optional and can be moved to the bottom, removed completely, or replaced with the WordPress `get_search_form()` function.

    <div id="logoWrapper">

        <a href="/wp/" id="logo">Logo</a>
        <a href="javascript:void(0);" id="WP_ToggleMobileMenu">&nbsp;</a>

    </div>

    <div id="WP_MobileMenuWrapper">

        <div id="WP_MobileMenuSearch">
        <form name="Search" id="searchform" action="/wp/" method="get">
        <input type="hidden" name="post_type" value="post" />
        <input type="text" name="s" id="s" placeholder="Search..." value="<?php echo isset($_GET['s']) ? get_search_query() : ""; ?>" autocomplete="off" />
        <input type="submit" id="searchsubmit" value="Go" />
        </form>
        <div class="clearer"></div>
        </div>

        <?php

            $aMenuOptions = array();
            $aMenuOptions['menu'] = "WP_MobileMenu";
            $aMenuOptions['menu_id'] = "WP_MobileMenu";

            wp_nav_menu($aMenuOptions);

        ?>

        <div id="WP_MobileMenuOptions">
        <div id="WP_BackToTop"><span>&uarr;</span><a href="#top">Back to Top</a></div>
        <div id="WP_CloseMobileMenu"><a href="#">Close</a><span>&uarr;</span></div>
        <div class="clearer"></div>
        </div>

    </div>
